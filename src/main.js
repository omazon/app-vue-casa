import Vue from 'vue'
import App from './App.vue'
import es from 'vee-validate/dist/locale/es'

//COMPONENTES GLOBALES AXIOS
window.axios = require ('axios');
//FIN DE AXIOS
import VeeValidate,{Validator} from 'vee-validate'

Validator.addLocale(es);
Vue.use(VeeValidate,{
	locale:'es'
});

new Vue({
  el: '#app',
  render: h => h(App)
})
